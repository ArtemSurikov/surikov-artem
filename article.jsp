<%--
    Document   : article
    Created on : 09.12.2021, 12:04:28
    Author     : root
--%>
 
<%@page contentType="text/html" pageEncoding="UTF-8"%>   
        <div id="main">
            <aside class="leftAside">
                <h2>Акции и скидки</h2>
                <ul>
                    <li><a href="#">Микроблейдинг бровей</a></li>
                    <li><a href="#">Маникюр и гель лак</a></li>
                    <li><a href="#">День красоты</a></li>
                    <li><a href="#">Наращивание ресниц</a></li>
                    
                </ul>
            </aside>
            <section>
                <article>
                    <h1>Услуги наших мастеров</h1>
                    <div class="text-article">
                        Ламинирование, креативное выпрямление волос;
                        Наращивание волос;
                        Плетение кос, афрокосичек, создание дред и др.;
                        Создание имиджа и стиля;
                        Маникюр и педикюр;
                        Биопилинг кожи головы и лечение волос;
                    </div>
                    <div class="fotter-article">
                        <span class="autor">Мастер специалист: <a href="#">Людмилина Л.В. и Богданов Б.В.</a></span>
                        <span class="date-article">Запись на приём: с 9.12.2021</span>
                    </div>
                </article>
            </section>
        </div>        
    </body>
</html>
 
