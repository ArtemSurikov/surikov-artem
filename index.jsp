<%--
    Document   : index
    Created on : 08.12.2021, 23:03:13
    Author     : root
--%>
 
<%@page contentType="text/html" pageEncoding="UTF-8"%>   
        <div id="main">
            <aside class="leftAside">
                <h2>Акции и скидки</h2>
                <ul>
                    <li><a href="#">Микроблейдинг бровей</a></li>
                    <li><a href="#">Маникюр и гель лак</a></li>
                    <li><a href="#">День красоты</a></li>
                    <li><a href="#">Наращивание ресниц</a></li>
                </ul>
            </aside>
            <section>
                <article>
                    <h1>Парикмахерская "Людмила"</h1>
                    <div class="text-article">
                        Доступные услуги для населения по уходу за волосами
                    </div>
                    <div class="fotter-article">
                        <span class="autor">Мастер специалист: <a href="#">Людмилина Л.В.</a></span>
                        <span class="read"><a href="javascript:void(0);">Читать...</a></span>
                        <span class="date-article">Запись на приём: с 9.12.2021</span>
                    </div>
                </article>
                <article>
                    <h1>Салон красоты "Алмаз"</h1>
                    <div class="text-article">
                        Косметическое обслуживание мужчин и женщин.
                        Бонусы и скидки при регистрации.
                    </div>
                    <div class="fotter-article">
                        <span class="autor">Мастер специалист: <a href="#">Богданов Б.В.</a></span>
                        <span class="read"><a href="javascript:void(0);">Читать...</a></span>
                        <span class="date-article">Запись на приём: с 9.12.2021</span>
                    </div>
                </article>
            </section>
        </div>
         
